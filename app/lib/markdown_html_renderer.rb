# frozen_string_literal: true

class MarkdownHtmlRenderer < Redcarpet::Render::Safe
  def initialize(extensions = {})
    @entities = extensions.delete(:entities)
    @autoplay = extensions.delete(:autoplay)

    super
  end

  def block_code(_, _)
    nil
  end

  def footnotes(_)
    nil
  end

  def footnote_def(_, _)
    nil
  end

  def footnote_ref(_)
    nil
  end

  def table(_, _)
    nil
  end

  def table_row(_)
    nil
  end

  def table_cell(_, _, _)
    nil
  end

  def hrule
    nil
  end

  def image(_, _, _)
    nil
  end

  def highlight(text)
    nil
  end

  def normal_text(text)
    html = Formatter.instance.send(:encode_and_link_urls, text, @entities[:accounts])
    html = Formatter.instance.send(:encode_custom_emojis, html, @entities[:custom_emojis], @autoplay) if @entities[:custom_emojis]
    html
  end
end

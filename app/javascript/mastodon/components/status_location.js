import React from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

export default class StatusLocation extends React.PureComponent {
    constructor(props) {
        super(props);
		this.toggleMap = this.toggleMap.bind(this);
    }

	static propTypes = {
		status: ImmutablePropTypes.map.isRequired,
		locations: PropTypes.object
		//location: PropTypes.map
	}

  componentDidMount () {
	  if (typeof this.props.status.get('location') !== 'undefined') {
		this.initMap();
	  }
  }

	initMap = () => {
		var statusId = this.props.status.get('id');
		var long = this.props.status.getIn(['location', 'geometry', 'coordinates', 0]);
		var lat = this.props.status.getIn(['location', 'geometry', 'coordinates', 1]);
		const myLatLng = { lat: lat, lng: long };
		var that = this;
		const google = window.google;
		var map = new google.maps.Map(
		document.querySelectorAll('[map-id="'+statusId+'"]')[0], {
      disableDefaultUI: true,
			zoom: 15,
			center: {lat:lat , lng:long},
			styles: [
				{
					"featureType": "administrative",
					"elementType": "labels",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "administrative.country",
					"elementType": "geometry.stroke",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "administrative.province",
					"elementType": "geometry.stroke",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "landscape",
					"elementType": "geometry",
					"stylers": [
						{
							"visibility": "off"
						},
						{
							"color": "#e3e3e3"
						}
					]
				},
				{
					"featureType": "landscape.natural",
					"elementType": "labels",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "road",
					"elementType": "all",
					"stylers": [
						{
							"color": "#cccccc"
						}
					]
				},
				{
					"featureType": "transit",
					"elementType": "labels.icon",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "water",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#FFFFFF"
						}
					]
				}
			]
		});

		new google.maps.Marker({
			position: myLatLng,
			map,
		});
  }

  toggleMap(e, statusId) {
    e.preventDefault();
		var mapContainer = document.querySelectorAll('[map-id="'+statusId+'"]');
		mapContainer[0].classList.toggle("show");
		//var spanElements = e.currentTarget.children;
		//var i;
		//for (i = 0; i < spanElements.length; i++) {
		//	spanElements[i].classList.toggle("show");
		//}
		//console.log(this);
	}

  render () {
	  const { status } = this.props;
	  const { locations } = this.props;
	  var statusId = status.get('id');
	  if (typeof status.get('location') !== 'undefined') {
		var statusLocation =  status.getIn(['location', 'properties', 'name']);
		return (
		<div className="location-container">
			<div className="location-name toggle-location-btn" onClick={(e) => {this.toggleMap(e, statusId)} }>
        <i className="fa fa-map-marker"></i>
				{statusLocation ? <span   style={{marginRight:'20px'}}>{statusLocation}</span> : ''}
			</div>
			<div className="status-map" map-id={statusId}></div>
		</div>
      );
    } else {
      return (
		<div></div>
      );
    }
  }

}

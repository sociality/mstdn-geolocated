import React from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';
import Icon from 'mastodon/components/icon';

const url = window.location.host;		

		
export default class ShareButtons extends React.PureComponent {	
	static propTypes = {
		content: PropTypes.object,
		statusId: PropTypes.string
	}
	
	constructor (props){
	  super(props);
	  this.shareFb = this.shareFb.bind(this);
	  this.shareTwitter = this.shareTwitter.bind(this);
	  this.state = {
			linkUrl: '',
		};
	  
	}
	
	componentDidMount () {
		this.setState({linkUrl: window.location.origin +'/web/statuses/' + this.props.statusId});
	}
	
	shareFb = e => {
		e.preventDefault();
		window.open('https://facebook.com/sharer/sharer.php?u='+this.state.linkUrl,'Facebook','width=500,height=600');
	}
	
	shareTwitter = e => {
		e.preventDefault();
		const tootHtmlContent = this.props.content.__html;
		const tootContent = tootHtmlContent.replace(/<(.|\n)*?>/g, '');		
		window.open('https://twitter.com/intent/tweet/?text='+tootContent+'@InAthens '+this.state.linkUrl+'&amp;url='+this.state.linkUrl,'Twitter','width=500,height=600');
	}
	
	shareLinkedIn = e => {
		e.preventDefault();
		const tootHtmlContent = this.props.content.__html;
		const tootContent = tootHtmlContent.replace(/<(.|\n)*?>/g, '');		
		window.open('https://www.linkedin.com/shareArticle?mini=true&amp;url='+this.state.linkUrl+'&amp;title=&amp;summary='+tootContent+'&amp;source='+this.state.linkUrl, 'LinkedIn','width=500,height=600');
	}
	
	shareReddit = e => {
		e.preventDefault();	
		window.open('https://reddit.com/submit/?url='+this.state.linkUrl+'&amp;resubmit=true&amp;title=InAthens', 'Reddit','width=500,height=600');
	}

  render () {		
		const tootHtmlContent = this.props.content.__html;
		const tootContent = tootHtmlContent.replace(/<(.|\n)*?>/g, '');
		return (
		<div className="share-container">
			<div className="share-buttons">
				<a href={'https://facebook.com/sharer/sharer.php?u='+this.state.linkUrl+''} onClick={this.shareFb} target="_blank" rel="nofollow"><Icon className='share_icon icon-button' id='facebook' fixedWidth /></a>
				<a href={'https://twitter.com/intent/tweet/?text='+tootContent+'@InAthens '+this.state.linkUrl+'&amp;url='+this.state.linkUrl+''}  onClick={this.shareTwitter} target="popup" rel="nofollow"><Icon className='share_icon icon-button' id='twitter' fixedWidth /></a>
				<a href={'https://www.linkedin.com/shareArticle?mini=true&amp;url=&amp;title=&amp;summary='+tootContent+'&amp;source='+this.state.linkUrl} onClick={this.shareLinkedIn} target="_blank" rel="nofollow"><Icon className='share_icon icon-button' id='linkedin' fixedWidth /></a>
				<a href={'https://reddit.com/submit/?url='+this.state.linkUrl+'&amp;resubmit=true&amp;title=InAthens'} onClick={this.shareReddit} target="_blank" rel="nofollow" ><Icon className='share_icon icon-button' id='reddit' fixedWidth /></a>
			</div>
		</div>
		);

  }

}
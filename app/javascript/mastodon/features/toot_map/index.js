import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { fetchBookmarkedStatuses, expandBookmarkedStatuses } from '../../actions/bookmarks';
import Column from '../ui/components/column';
import ColumnHeader from '../../components/column_header';
import { addColumn, removeColumn, moveColumn } from '../../actions/columns';
import StatusList from '../../components/status_list';
import { defineMessages, injectIntl, FormattedMessage } from 'react-intl';
import ImmutablePureComponent from 'react-immutable-pure-component';
import { debounce } from 'lodash';

import { Map as ImmutableMap, List as ImmutableList } from 'immutable';
import { withRouter } from 'react-router-dom';
import styles from '../../../packs/datepicker.min.css';
import datepicker from '../../../packs/datepicker.min.js';
import Button from '../../components/button';

const messages = defineMessages({
  heading: { id: 'column.tootmap', defaultMessage: 'Map' },
  placeholder_from: { id: 'toot_map.placeholder_from', defaultMessage: 'From' },
  placeholder_to: { id: 'toot_map.placeholder_to', defaultMessage: 'To' },
  date_filter: { id: 'toot_map.date_filter', defaultMessage: 'Dates' },
  hastag_input: { id: 'toot_map.hastag_input', defaultMessage: '#hastag' },
  filter_button: { id: 'toot_map.filter_button', defaultMessage: 'Filter' },
});



const mapStateToProps = state => ({
  //statusIds: state.getIn(['timelines', 'home', 'items']),
  //statusIds: state.getIn(['timelines']),
  //statusIds: state.get('statuses'),
  //statusIds: getStatusIds(state, { type: 'public' }),
  statusIds: state.getIn(['timelines', 'public', 'items']),
  //statusIds: state.getIn(['status_lists', 'bookmarks', 'items']),
  isLoading: state.getIn(['status_lists', 'bookmarks', 'isLoading'], true),
  hasMore: !!state.getIn(['status_lists', 'bookmarks', 'next']),
  mapLoaded: state.getIn(['map', 'map_script_loaded']),
  mstate: state,
  statuses: state.getIn(['statuses', 'items']),
});


var dateStart, dateEnd;

export default @connect(mapStateToProps)
//export default @connect(makeMapStateToProps)
@injectIntl
@withRouter
class TootMap extends ImmutablePureComponent {
	constructor (props){
		super(props);
	    this.state = {
			mapPrinted: false,
        };
		this.loadDatepicker = this.loadDatepicker.bind(this);
	}

  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    shouldUpdateScroll: PropTypes.func,
    statusIds: ImmutablePropTypes.list.isRequired,
	//statusIds,
    intl: PropTypes.object.isRequired,
    columnId: PropTypes.string,
    multiColumn: PropTypes.bool,
    hasMore: PropTypes.bool,
    isLoading: PropTypes.bool,
	mapLoaded: PropTypes.bool,
	mstate:PropTypes.object,
	statuses:PropTypes.object,

  };

  componentWillMount () {
    this.props.dispatch(fetchBookmarkedStatuses());
	//console.log(statusIds);
  }

  componentDidMount () {
	console.log(this.props.statusIds);
	var that = this;
	document.addEventListener('click',function(e){
		if(e.target && e.target.id== 'infowindow_link'){
				e.preventDefault();
				var statusId = e.target.getAttribute("data-id");
				that.props.history.push('/statuses/' + statusId);
				//that.props.history.push('/statuses/106890303014110003');
		 }
	 });

	//console.log(this.props.mstate);

	if(this.props.mapLoaded && this.props.statusIds && (Object.keys(this.props.statusIds).length > 0)) {
		this.initMap(this.props.statusIds);
	}

	this.loadDatepicker();
  }

  componentDidUpdate (prevProps) {
	  console.log(this.props.statusIds);
		if (this.props.mapLoaded !== prevProps.mapLoaded) {
			if(this.props.statusIds && (this.props.statusIds.size > 0)) {
				//console.log('map prevProps statusIds');
				//console.log(this.props.statusIds.size);
				this.initMap(this.props.statusIds);
			}
	  }
	  if ((this.props.statusIds !== prevProps.statusIds)&& (this.props.statusIds.size > 0) && !this.state.mapPrinted) {
		  if (this.props.mapLoaded ) {
			//console.log('map mapLoaded');
			//console.log(this.props.statusIds.size);
			this.initMap(this.props.statusIds);
			this.setState({mapPrinted:true});
		  }
	  }

  }

  componentWillUnmount() {
	  dateStart.remove();
	  dateEnd.remove();
  }

	loadDatepicker () {
	  console.log('filter');
	  const options = {
			// Event callbacks.
		  onSelect: instance => {
			// Show which date was selected.
			//console.log(instance.dateSelected)
		  },
		  // Customizations.
		  formatter: (input, date, instance) => {
			var curr_date = date.getDate();
			var curr_month = date.getMonth() + 1; //Months are zero based
			var curr_year = date.getFullYear();
			//input.value = curr_date + "/" + curr_month + "/" + curr_year;
			input.value = date.toLocaleDateString("en-GB", { // you can use undefined as first argument
			  year: "numeric",
			  month: "2-digit",
			  day: "2-digit",
			});
		  },
		  customDays: ['Κ', 'Δ', 'Τ', 'Τ', 'Π', 'Π', 'Σ'],
			customMonths: ['Ιαν', 'Φεβ', 'Μαρ', 'Απρ', 'Μάι', 'Ιούν', 'Ιούλ', 'Αύγ', 'Σεπ', 'Οκτ', 'Νοέμ', 'Δεκ'],
		 id: 1,
	  }
	  dateStart = datepicker('#datepicker_from', options);
	  dateEnd = datepicker('#datepicker_to', options)
	}

	  filterMap (e) {
		  var startDate = document.getElementById('datepicker_from').value;
		  var endDate = document.getElementById('datepicker_to').value;
      var hastag = document.getElementById('hashtag-filter-input').value;
      var hastagClean = hastag.replace('#','');
		  //console.log(startDate);
		  if (startDate.length > 0) {
			const startDateArray = startDate.split("/");
			startDate = startDateArray[2] + ', ' + startDateArray[1] + ', ' + startDateArray[0];
			var startDateTimestamp = (new Date(startDate)).getTime();
			//console.log(startDateTimestamp);
		  }
		  if (endDate.length > 0) {
			const endDateArray = endDate.split("/");
			endDate = endDateArray[2] + ', ' + endDateArray[1] + ', ' + endDateArray[0];
			var endDateTimestamp = (new Date(endDate)).getTime() + 86400000; // 86400000 equals to 24hours
			//console.log(endDateTimestamp);
		  }

			var newStatusIds = [];
			var state = this.props.mstate;
			//console.log(state);
			this.props.statusIds.filter(function(statusId) {
			  if (statusId == null) {
				return false; // skip
			  }
			  return true;
			}).map((statusId, index) => {

        var includeStatus = false;
				//console.log(statusId);
				var toot = state.getIn(['statuses', statusId]);
				//console.log(toot);
				var tootDate =  toot.getIn(['created_at']);
				//console.log(tootDate);
				var tootDateTimestamp = (new Date(tootDate)).getTime();
				//console.log(tootDateTimestamp);

        //Check Date
				if ((startDate.length > 0) && (endDate.length > 0)) {
					if ((tootDateTimestamp >= startDateTimestamp) && (tootDateTimestamp < endDateTimestamp)) {
					       includeStatus = true;
					}
				} else if (startDate.length > 0) {
					if (tootDateTimestamp >= startDateTimestamp) {
					       includeStatus = true;
					}
				} else if (endDate.length > 0) {
					if (tootDateTimestamp <= endDateTimestamp) {
					       includeStatus = true;
					}
				}else{
          includeStatus = true;
        }

        //Check Hastag
        if(includeStatus){
          if(hastag.length > 0){
            var tags = toot.getIn(['tags']);
            var tagPresent = false;
            if(tags.size==0){
             //Do nothing
            }else{
              var i = 0;
              while (i<tags.size) {
                var name = toot.getIn(['tags',i,'name'])
                if(name==hastagClean){
                     tagPresent = true;
                }
                i++;
              }
           }
            if(tagPresent==false){
                includeStatus = false;
            }
          }
        }

        //Send to map
        if(includeStatus){
          newStatusIds.push(statusId);
        }

			});
			this.initMap(newStatusIds);

	  }

	initMap = (statusIds) => {
		var latArray = [];
		var longArray = [];
		var that = this;
		const google = window.google;
		//console.log('statusIds');
		//console.log(this.props.statusIds);
		//var statusIds = this.props.statusIds;
		var state = this.props.mstate;
		const infowindow = new google.maps.InfoWindow();
		statusIds.map((statusId, index) => {
			console.log(statusId);
			var tootLocation = state.getIn(['statuses', statusId, 'location']);
			var toot = state.getIn(['statuses', statusId]);
			if (typeof tootLocation !== 'undefined') {
				var long = toot.getIn(['location', 'geometry', 'coordinates', 0]);
				var lat = toot.getIn(['location', 'geometry', 'coordinates', 1]);
				longArray.push(long);
				latArray.push(lat);
			}
		});

		const longSum = longArray.reduce((a, b) => a + b, 0);
		const longAvg = (longSum / longArray.length) || 0;
		const latSum = latArray.reduce((a, b) => a + b, 0);
		const latAvg = (latSum / latArray.length) || 0;
	  //console.log(longAvg, latAvg);
		var map = new google.maps.Map(
		document.getElementById('toot_map'), {
			zoom: 13,
			//center: {lat:latAvg , lng:longAvg},
			center: {lat: 37.985527 , lng:23.732644},
			styles: [
				{
					"featureType": "administrative",
					"elementType": "labels",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "administrative.country",
					"elementType": "geometry.stroke",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "administrative.province",
					"elementType": "geometry.stroke",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "landscape",
					"elementType": "geometry",
					"stylers": [
						{
							"visibility": "on"
						},
						{
							"color": "#e3e3e3"
						}
					]
				},
				{
					"featureType": "landscape.natural",
					"elementType": "labels",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "road",
					"elementType": "all",
					"stylers": [
						{
							"color": "#cccccc"
						}
					]
				},
				{
					"featureType": "transit",
					"elementType": "labels.icon",
					"stylers": [
						{
							"visibility": "off"
						}
					]
				},
				{
					"featureType": "water",
					"elementType": "geometry",
					"stylers": [
						{
							"color": "#FFFFFF"
						}
					]
				}
			]
		});

		statusIds.map((statusId, index) => {
			//console.log(statusId);
			var tootLocation = state.getIn(['statuses', statusId, 'location']);
			var toot = state.getIn(['statuses', statusId]);
		  //console.log(toot);
			//console.log(tootLocation);
			if (typeof tootLocation !== 'undefined') {
				//console.log('location');
				//console.log(tootLocation);
        var content =  toot.getIn(['content']);
        var excerpt = content.replace(/(<([^>]+)>)/gi, "");
        var trimmedExcerpt = excerpt.substring(0, 100)
        if(excerpt.length>100){
           trimmedExcerpt =  trimmedExcerpt+'...'
        }
				var long = toot.getIn(['location', 'geometry', 'coordinates', 0]);
				var lat = toot.getIn(['location', 'geometry', 'coordinates', 1]);
				var tootLocation =  toot.getIn(['location', 'properties', 'name']);
				var myLatLng = { lat: lat, lng: long };
				const marker = new google.maps.Marker({
					position: myLatLng,
					map,
				});
				google.maps.event.addListener(marker, 'click', function(){
					infowindow.close(); // Close previously opened infowindow
					infowindow.setContent('<div id="infowindow"><a id="infowindow_link" target="_blank" href="statuses/'+ statusId +'"  data-id="'+ statusId +'">'+'<p>"'+ trimmedExcerpt+'"</p><span class="pop-loc"><i class="fa fa-map-marker"></i> '+
          tootLocation +'</span></a></div>');
					infowindow.open(map, marker);
				});
			}

		});

  }


  handlePin = () => {
    const { columnId, dispatch } = this.props;

    if (columnId) {
      dispatch(removeColumn(columnId));
    } else {
      dispatch(addColumn('TOOTMAP', {}));
    }
  }

  handleMove = (dir) => {
    const { columnId, dispatch } = this.props;
    dispatch(moveColumn(columnId, dir));
  }

  handleHeaderClick = () => {
    this.column.scrollTop();
  }

  setRef = c => {
    this.column = c;
  }

  handleLoadMore = debounce(() => {
    this.props.dispatch(expandBookmarkedStatuses());
  }, 300, { leading: true })

  render () {
    const { intl, shouldUpdateScroll, statusIds, columnId, multiColumn, hasMore, isLoading } = this.props;
    const pinned = !!columnId;

    const emptyMessage = <FormattedMessage id='empty_column.bookmarked_statuses' defaultMessage="No toots found." />;

    return (
      <Column bindToDocument={!multiColumn} ref={this.setRef} label={intl.formatMessage(messages.heading)}>
        <ColumnHeader
          icon='map'
          title={intl.formatMessage(messages.heading)}
          onPin={this.handlePin}
          onMove={this.handleMove}
          onClick={this.handleHeaderClick}
          pinned={pinned}
          multiColumn={multiColumn}
          showBackButton
        />
		<div className='map-filter'>
        <label>{intl.formatMessage(messages.date_filter)}</label>
        <div className="date-filter">
				    <input id='datepicker_from' placeholder={intl.formatMessage(messages.placeholder_from)} />
				    <input id='datepicker_to' placeholder={intl.formatMessage(messages.placeholder_to)} />
        </div>
        <div className="hashtag-filter">
          <input id="hashtag-filter-input" placeholder={intl.formatMessage(messages.hastag_input)} />
        </div>
				<Button text={intl.formatMessage(messages.filter_button)} onClick={(e) => {this.filterMap(e)} } block />
		</div>
		<div id='toot_map'>
		</div>

      </Column>
    );
  }

}

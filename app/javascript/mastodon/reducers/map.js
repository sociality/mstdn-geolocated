import { LOAD_SCRIPT } from '../actions/map';
import { Map as ImmutableMap } from 'immutable';

const initialState = ImmutableMap({
  map_script_loaded: false,
});

export default function map(state = initialState, action) {
  switch(action.type) {
  case LOAD_SCRIPT:
    return state.set('map_script_loaded', true);
  default:
    return state;
  }
};

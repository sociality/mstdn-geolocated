class AddPlaceNameToStatuses < ActiveRecord::Migration[6.1]
  def change
    add_column :statuses, :place_name, :string
  end
end

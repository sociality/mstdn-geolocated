class AddLocationToStatuses < ActiveRecord::Migration[6.1]
  def change
    add_column :statuses, :longitude, :numeric
    add_column :statuses, :latitude, :numeric
  end
end
